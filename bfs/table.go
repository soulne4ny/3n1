package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	N := uint(10000)
	if len(os.Args) > 1 && os.Args[1] != "" {
		if n, e := strconv.ParseUint(os.Args[1], 10, 64); e == nil {
			N = uint(n)
		} else {
			panic(e)
		}
	}

	N++

	numbers := make([]uint, 0, N)
	g := make([]uint, N)
	b := make([]uint, N)

	numbers = append(numbers, 1)
	g[1] = 1
	maxG := g[1]
	b[1] = 1

	for j := uint(0); j < uint(len(numbers)); j++ {
		for i := numbers[j] << 1; i < 3*N+1; i <<= 1 {
			if i > 4 && (i-1)%3 == 0 && (i-1)/3%2 != 0 {
				k := (i - 1) / 3
				if g[k] > 0 {
					panic("repeated")
				} else {
					numbers = append(numbers, k)
					g[k] = g[numbers[j]] + 1
					b[k] = numbers[j]
					if maxG < g[k] {
						maxG = g[k]
					}
				}
			}
		}
	}

	for l := uint(1); l <= maxG; l++ {
		fmt.Printf("%7d:", l)
		w := 0
		for i := uint(1); i < N; i += 2 {
			if g[i] == l {
				if w == 5 {
					fmt.Printf("\n%8s", "")
					w = 1
				}
				fmt.Printf("%8d%-8s", i, fmt.Sprintf(" [%d]", b[i]))
				w++
			}
		}
		fmt.Printf("\n\n")
	}

	//fmt.Printf("\n not reached: ")
	//for i := uint(1); i < N; i += 2 {
	//	if g[i] == 0 {
	//		fmt.Printf(" %d", i)
	//	}
	//}
	//fmt.Printf("\n")
}
