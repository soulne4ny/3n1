package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	N := uint(10000)
	if len(os.Args) > 1 && os.Args[1] != "" {
		if n, e := strconv.ParseUint(os.Args[1], 10, 64); e == nil {
			N = uint(n)
		} else {
			panic(e)
		}
	}

	N++

	numbers := make([]uint, 0, N)
	g := make([]uint, N)
	b := make([]uint, N)

	numbers = append(numbers, 1)
	g[1] = 1
	maxG := g[1]
	b[1] = 1

	for j := uint(0); j < uint(len(numbers)); j++ {
		for i := numbers[j] << 1; i < 3*N+1; i <<= 1 {
			if i > 4 && (i-1)%3 == 0 && (i-1)/3%2 != 0 {
				k := (i - 1) / 3
				if g[k] > 0 {
					panic("repeated")
				} else {
					numbers = append(numbers, k)
					g[k] = g[numbers[j]] + 1
					b[k] = numbers[j]
					if maxG < g[k] {
						maxG = g[k]
					}
				}
			}
		}
	}

	fmt.Printf("graph {\n")
	fmt.Printf(" rankdir=LR;\n")
	fmt.Printf(" subgraph generations {\n")
	fmt.Printf("  label=\"generations\";\n")
	fmt.Printf("  G%d [shape=box,color=red];\n", 1)
	for l := uint(2); l <= maxG; l++ {
		fmt.Printf("  G%d [shape=box,color=red];\n", l)
		fmt.Printf("  G%d--G%d;\n", l-1, l)
	}
	fmt.Printf(" }\n")
	for l := uint(1); l <= maxG; l++ {
		fmt.Printf("G%d -- {", l)
		for i := uint(1); i < N; i += 2 {
			if g[i] == l {
				fmt.Printf(" %d", i)
			}
		}
		fmt.Printf(" }\n")
	}
	for i := uint(1); i < N; i += 2 {
		if g[i] == 0 {
			fmt.Printf("%d\n", i)
		}
	}
	fmt.Printf("}\n")
}
