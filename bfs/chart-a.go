package main

import (
	"fmt"
	"strconv"
)

const N = 10000

func main() {
	numbers := make([]uint, 0, N+2)
	g := make([]uint, N+2)

	numbers = append(numbers, 1)
	g[1] = 1

	for j := 0; j < len(numbers); j++ {
		for i := numbers[j] << 1; i < 3*N+1; i <<= 1 {
			//fmt.Printf("[%d/%d] %d i %d\n", j, len(numbers), numbers[j], i)
			if i > 4 && (i-1)%3 == 0 && (i-1)%6 != 0 {
				k := (i - 1) / 3
				//if k > N {
				//	fmt.Printf("zhopa [%d] %d %d %d %d\n", j, numbers[j], i, k, N)
				//	break
				//} else
				if g[k] > 0 {
					//fmt.Printf("R %d  [%d] %d  [%d] %d\n", k, g[k], numbers[g[k]], j, numbers[j])
					fmt.Printf("%d\t%d\t[%d]\t%d\t[%d]\n", k, numbers[j], j, numbers[g[k]], g[k])
				} else {
					numbers = append(numbers, k)
					g[k] = g[numbers[j]] + 1
					//fmt.Printf("numbers <- k %d : len %d\n", k, len(numbers))
				}
			}
		}
	}

	max := uint(0)
	mask := ^uint64(0)
	a := ' '
	b := ' '
	for n := uint64(1); n < N; n += 2 {
		if g[n] > 0 {
			if max < g[n] {
				max = g[n]
				a = '*'
			} else {
				a = ' '
			}
			if mask&(uint64(1)<<g[n]) == 0 {
				b = ' '
			} else {
				mask &= ^(uint64(1) << g[n])
				b = '#'
			}
			fmt.Printf("%c%c%6d :%*s: %d (0b%b) (0t%s)\n", a, b, g[n], g[n], "", n, n, strconv.FormatUint(n, 3))
		} else {
			fmt.Printf("%80s !!!%d (0b%b) (0t%s)\n", "", n, n, strconv.FormatUint(n, 3))
		}
	}
}
